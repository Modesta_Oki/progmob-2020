package ukdw.com.progmob_2020.Pertemuan6;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import ukdw.com.progmob_2020.R;

public class PrefActivity extends AppCompatActivity {
    String isLogin = "";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref = (Button)findViewById(R.id.btnPref);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences( "pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        androidx.appcompat.widget.Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        //Membaca pref islogin apakah true atau false
        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")){
            btnPref.setText("Logout");
        }else{
            btnPref.setText("Login");
        }

        //Pengisian pref
        btnPref.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                    btnPref.setText("Logout");
                }else {
                    editor.putString("isLogin", "0");
                    btnPref.setText("Login");
                }
                editor.commit();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.add_bar:
                currentLayout= (ConstraintLayout)findViewById(R.id.ConstraintLayoutpref);
                //Toast.makeText(this,"Tambah Sesuatu",Toast.LENGTH_LONG).show();
                Snackbar.make(currentLayout, "Tertekan sesuatu", Snackbar.LENGTH_LONG).show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

