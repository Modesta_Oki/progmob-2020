package ukdw.com.progmob_2020.CRUD;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.os.IResultReceiver;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import javax.security.auth.callback.Callback;
import retrofit2.Call;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatakuliahAddActivity  extends AppCompatActivity {
    ProgressDialog mk;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matakuliah_add);

        EditText edNama = (EditText)findViewById(R.id.editNama);
        EditText edKode= (EditText)findViewById(R.id.editKode);
        EditText edhari= (EditText)findViewById(R.id.editHari);
        EditText edSesi = (EditText)findViewById(R.id.editSesi);
        EditText edSKS = (EditText)findViewById(R.id.editSKS);
        Button btnSave = (Button)findViewById(R.id.btnSave);


        mk = new ProgressDialog(MatakuliahAddActivity.this);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mk.setTitle("Please Wait");
                mk.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_matakuliah(
                        edNama.getText().toString(),
                        edKode.getText().toString(),
                        edhari.getText().toString(),
                        edSesi.getText().toString(),
                        edSKS.getText().toString(),
                        "",
                        "72180208"

                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        mk.dismiss();
                        Toast.makeText(MatakuliahAddActivity.this,"Success", Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        mk.dismiss();
                        Toast.makeText(MatakuliahAddActivity.this, "Error",Toast.LENGTH_LONG);
                    }
                });
            }
        });
    }
}