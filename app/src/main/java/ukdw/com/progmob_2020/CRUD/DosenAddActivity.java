package ukdw.com.progmob_2020.CRUD;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenAddActivity extends AppCompatActivity {
    ProgressDialog Dos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_add);

        EditText edNama = (EditText)findViewById(R.id.editTextNama);
        EditText edNidn = (EditText)findViewById(R.id.editTextNidn);
        EditText edAlamat = (EditText)findViewById(R.id.editTextAlamat);
        EditText edEmail = (EditText)findViewById(R.id.editTextEmail);
        EditText edGelar = (EditText)findViewById(R.id.editTextGelar);
        Button btnAddDosen = (Button)findViewById(R.id.buttonAddDosen);
        Dos = new ProgressDialog(DosenAddActivity.this);

        btnAddDosen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dos.setTitle("Please Wait");
                Dos.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.add_dosen(
                        edNama.getText().toString(),
                        edNidn.getText().toString(),
                        edAlamat.getText().toString(),
                        edEmail.getText().toString(),
                        edGelar.getText().toString(),
                        "Leave It Empty",
                       "72180208"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        Dos.dismiss();
                        Toast.makeText(DosenAddActivity.this, "DATA BERHASIL DISIMPAN", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        Dos.dismiss();
                        Toast.makeText(DosenAddActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}

