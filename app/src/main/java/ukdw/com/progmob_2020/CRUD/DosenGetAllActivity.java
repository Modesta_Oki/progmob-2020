package ukdw.com.progmob_2020.CRUD;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Adapter.DosenCRUDRecyclerAdapter;
import ukdw.com.progmob_2020.Model.Dosen;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class DosenGetAllActivity extends AppCompatActivity {

    RecyclerView rvDosenAll;
    DosenCRUDRecyclerAdapter DosenAdapter;
    ProgressDialog Dos;
    List<Dosen> dosenList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dosen_get_all);

        rvDosenAll = (RecyclerView)findViewById(R.id.rvDosenAll);
        Dos = new ProgressDialog(this);
        Dos.setTitle("Please Wait");
        Dos.show();

        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Dosen>> call = service.getDosen("72180208");

        call.enqueue(new Callback<List<Dosen>>() {
            @Override
            public void onResponse(retrofit2.Call<List<Dosen>> call, Response<List<Dosen>> response) {
                Dos.dismiss();
                dosenList= response.body();
                DosenAdapter = new DosenCRUDRecyclerAdapter((Context) dosenList);

                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(DosenGetAllActivity.this);
                rvDosenAll.setLayoutManager(layoutManager);
                rvDosenAll.setAdapter(DosenAdapter);
            }

            @Override
            public void onFailure(retrofit2.Call<List<Dosen>> call, Throwable t) {
                Dos.dismiss();
                Toast.makeText(DosenGetAllActivity.this, "Error", Toast.LENGTH_LONG);
            }
        });

    }
}


