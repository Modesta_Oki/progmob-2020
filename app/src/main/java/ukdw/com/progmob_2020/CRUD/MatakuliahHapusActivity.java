package ukdw.com.progmob_2020.CRUD;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob_2020.Model.DefaultResult;
import ukdw.com.progmob_2020.Network.GetDataService;
import ukdw.com.progmob_2020.Network.RetrofitClientInstance;
import ukdw.com.progmob_2020.R;

public class MatakuliahHapusActivity extends AppCompatActivity {
    ProgressDialog mk;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_matkul);
        EditText edKode = (EditText)findViewById(R.id.editKode);
        Button btnHapus = (Button)findViewById(R.id.btnHapus);
        mk = new ProgressDialog(MatakuliahHapusActivity.this);

        btnHapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mk.setTitle("Lagi Proses");
                mk.show();

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<DefaultResult> call = service.delete_dosen(
                        edKode.getText().toString(),
                        "72180237"
                );
                call.enqueue(new Callback<DefaultResult>() {
                    @Override
                    public void onResponse(Call<DefaultResult> call, Response<DefaultResult> response) {
                        mk.dismiss();
                        Toast.makeText(MatakuliahHapusActivity
                                .this, "DATA BERHASIL DIHAPUS", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<DefaultResult> call, Throwable t) {
                        mk.dismiss();
                        Toast.makeText(MatakuliahHapusActivity.this, "GAGAL", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

    }
}

