package ukdw.com.progmob_2020.Model;

public class MatakuliahDebugging {
    private String kode;
    private String nama;
    private String hari;

    public MatakuliahDebugging(String kode, String nama, String hari) {
        this.kode = kode;
        this.nama = nama;
        this.hari = hari;
    }

    public String getKode() {return kode; }

    public void setKode(String kode) {this.kode = kode;}

    public String getNama() { return nama; }

    public void setNama(String nama) {this.nama = nama;}

    public String getHari() {return hari;}

    public void setHari(String hari) {this.hari = hari; }

}
