package ukdw.com.progmob_2020.Model;

public class DosenDebugging {
    private String nama;
    private String nidn;
    private String alamat;

    public DosenDebugging(String nama, String nidn, String alamat) {
        this.nama = nama;
        this.nidn = nidn;
        this.alamat = alamat;
    }

    public String getNama() {return nama;}

    public void setNama(String nama) {this.nama = nama;}

    public String getNidn() {return nidn; }

    public void setNidn(String nidn) {this.nidn = nidn;}

    public String getAlamat() {return alamat;}

    public void setAlamat(String alamat) {this.alamat = alamat;}

}
