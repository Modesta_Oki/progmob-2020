package ukdw.com.progmob_2020.Model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Matakuliah {
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("kode")
    @Expose
    private String kode;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("hari")
    @Expose
    private String hari;

    @SerializedName("sesi")
    @Expose
    private String sesi;

    @SerializedName("sks")
    @Expose
    private String sks;

    public Matakuliah(String kode, String nama, String hari){
        this.kode = kode;
        this.nama = nama;
        this.hari = hari;

    }

    public Matakuliah(String id, String kode, String nama, String hari, String sesi, String sks) {
        this.id = id;
        this.kode = kode;
        this.nama = nama;
        this.hari = hari;
        this.sesi = sesi;
        this.sks = sks;
    }

    public Matakuliah(String hari, String kode, String nama, String sesi, String sks) {
        this.hari = hari;
        this.kode = kode;
        this.nama = nama;
        this.sesi = sesi;
        this.sks = sks;
    }

    public String getId() {return id;}

    public void setId(String id) {this.id = id;}

    public String getKode() {return kode;}

    public void setKode(String kode) {this.kode = kode;}

    public String getNama() {return nama; }

    public void setNama(String nama) {this.nama = nama;}

    public String getHari() {return hari;   }

    public void setHari(String hari) {this.hari = hari;}

    public String getSesi() {return sesi;}

    public void setSesi(String sesi) {this.sesi = sesi;}

    public String getSks() {return sks;}

    public void setSks(String sks) {this.sks = sks;}

}

